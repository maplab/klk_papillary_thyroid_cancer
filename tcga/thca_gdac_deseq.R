#!/usr/bin/env R
##
## KLK algorithm BRU against other groups
##
## 17 oct 2016
## Izaskun Mallona

library(DESeq2)
library("BiocParallel")
library("pheatmap")
library("RColorBrewer")
library(Cairo)
library(ggplot2)
library(TCGAbiolinks) ## GEA GO analysis

NTHREADS <- 6
IMPPC <- '/imppc/labs/maplab/imallona'
WD <- '~/thca_gdac'
TMP <- file.path(WD, 'tmp')
DAT <- file.path(WD, 'dat')
SRC <- file.path('~')
OUTDIR <- file.path(WD, 'results')

## functions start

##' @param dat a deseq differential expression output
##' @return 
##' @author Izaskun Mallona
retrieve_significant <- function(dat) {
    dat <- dat[!is.na(dat$log2FoldChange) &
                   abs(dat$log2FoldChange) >2 &
                       !is.na(dat$padj) &
                           dat$padj < 0.05,]
    return(dat)
}

##' @param contrasts_results a list of contrasts run by deseq
##' @return 
##' @author Izaskun Mallona
bulk_gene_ontology <- function(contrasts_results) {
    for (name in names(contrasts_results)) {
        print(name)
        dat <- retrieve_significant(contrasts_results[[name]])
        genes <- rownames(dat)
        genes <- sapply(strsplit( genes, '|', fixed = TRUE), function(x) return(x[[1]]))

        ansEA <- TCGAanalyze_EAcomplete(TFname = name, genes)

        dir.create(file.path(OUTDIR, name), showWarnings = FALSE)

        write.csv(dat, file = file.path(OUTDIR, name, paste0(name,
                           'differentially_expressed_genes.csv')))

        setwd(file.path(OUTDIR, name))

        TCGAvisualize_EAbarplot(tf = rownames(ansEA$ResBP), 
                                GOBPTab = ansEA$ResBP,
                                GOCCTab = ansEA$ResCC,
                                GOMFTab = ansEA$ResMF,
                                PathTab = ansEA$ResPat,
                                nRGTab = genes, 
                                nBar = 20)

        setwd(WD)
    }
}

## functions end

register(MulticoreParam(NTHREADS))

dir.create(OUTDIR, recursive = TRUE)
dir.create(DAT)
dir.create(TMP)

setwd(WD)

annot <- read.table(file.path(SRC, 'data', 'KLK_NEW.txt'),
                    header = TRUE,  na.string = 'NA',
                    stringsAsFactors = FALSE, sep = "\t")

dat <- read.table(file.path(DAT, 'raw_counts.tsv'),
                  header = TRUE, sep ="\t", stringsAsFactors = FALSE)
genes <- dat[,1]
annot <- unique(annot)

colnames(dat)[2:ncol(dat)] <- gsub('\\.', '-', strtrim(colnames(dat)[2:ncol(dat)], 16))

if (length(unique(annot$Sample)) != nrow(annot))
    stop('1')

dat <- dat[,unique(annot$Sample)]

row.names(dat) <- genes

rounded <- round(dat)

annot$KLK_tree <- annot$Arbolnuevo

dds <- DESeqDataSetFromMatrix(countData = rounded,
                              colData = annot,
                              design = ~ KLK_tree)

dds.deseq <- DESeq(dds, parallel = TRUE)
save(dds.deseq, file = file.path(WD, 'dds.deseq.RData'))
save(dds, file = file.path(WD, 'dds.RData'))

vsd <- varianceStabilizingTransformation(dds.deseq, blind = TRUE)

for (item in colnames(colData(vsd))[1:ncol(colData(vsd))]) {
    print(item)
    foo <- plotPCA(vsd, intgroup = item)
    ggsave(filename = file.path(OUTDIR, paste0(item,'_pca.png')), plot = foo)

}

## contrasts

contrasts_results <- list()

contrasts <- list(
    list("KLK_treeBRAF.like", "KLK_treeBRU"),
    list("KLK_treeRAS.like", "KLK_treeBRU"),
    list("KLK_treeNT", "KLK_treeBRU"))

for (i in 1:length(contrasts)){

    contrasts_results[[paste(contrasts[[i]],
                             collapse = "__vs__")]] <- results(dds.deseq,
                                 contrast = contrasts[[i]])
}

save(contrasts_results, file = file.path(OUTDIR, 'contrasts_bru.RData'))

bulk_gene_ontology(contrasts_results)
