#!/bin/bash
##
## BRAF V600E, [NHK]RAS etc mutations fetcher
## THCA cohort
##
## Izaskun Mallona, GPL
## 15 feb 2017

HOME='/home/labs/maplab/imallona'
IMPPC='/imppc/labs/maplab/imallona'
WD="$HOME"/thca_gdac_checks
SRC="$IHOME"/src/thca_gdac/

cd "$WD"

echo 'Aggregated features analysis'
wget http://gdac.broadinstitute.org/runs/analyses__latest/reports/cancer/THCA/Aggregate_AnalysisFeatures/THCA-TP.samplefeatures.txt

echo 'MAF mutsig'

wget http://gdac.broadinstitute.org/runs/analyses__2016_01_28/reports/cancer/THCA-TP/MutSigNozzleReport2.0/THCA-TP.final_analysis_set.maf

fgrep V600E THCA-TP.final_analysis_set.maf  |less
cut -f1  THCA-TP.final_analysis_set.maf | less

echo 'Mutsig2 report'

wget http://gdac.broadinstitute.org/runs/analyses__2016_01_28/data/THCA-TP/20160128/gdac.broadinstitute.org_THCA-TP.MutSigNozzleReport2.0.Level_4.2016012800.0.0.tar.gz

tar xzvf gdac.broadinstitute.org_THCA-TP.MutSigNozzleReport2.0.Level_4.2016012800.0.0.tar.gz

find . -name THCA-TP.cosmic_mutations.txt | xargs head

## this looks nice, let's try to overlap it with the rbuj's cohort
## coding within the Rmd file
