#!/bin/bash
#
## Retrieves THCA cohort RNASeq data from GDAC
##
## Izaskun Mallona
## 16th Dec 2015

WD=~/thca_gdac_back
DAT=$WD/dat

mkdir -p $DAT

cd $DAT

wget http://gdac.broadinstitute.org/runs/stddata__2015_11_01/data/THCA/20151101/gdac.broadinstitute.org_THCA.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data.Level_3.2015110100.0.0.tar.gz

tar xzvf gdac.broadinstitute.org_THCA.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data.Level_3.2015110100.0.0.tar.gz

head -1 gdac.broadinstitute.org_THCA.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data.Level_3.2015110100.0.0/THCA.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data.data.txt | \
    awk '{sep = "\t"; print NF}'


which="1,""$(seq 2 3 1706 | tr '\n' ',')"
which=${which:0:${#which}-1}

cut -f"$which" gdac.broadinstitute.org_THCA.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data.Level_3.2015110100.0.0/THCA.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes__data.data.txt | awk '{if (NR!=2) {print}}'  > raw_counts.tsv
