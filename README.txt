Source code accompanying the manuscript Buj et al, 2017 (submitted)

Folders
   - Data, contains the data the KLK_script.R source refers to (i.e. to build figures and tables)   
   - data, further info
   - tcga, retrieval and tidying of expression and mutation data from TCGA/firehose

Filenames
   - Main source
       - KLK_script.R, main flow
   - Side source
       - tcga/thca_gdac_data_retrieval_for_rnaseq.sh Retrieves THCA GDAC RNASeq data (a gunzipped copy ofits output, the raw_counts.tsv, is available at the data folder)
       - tcga/thca_gdac_deseq.R Performs the DESeq2 contrasts according to the 'raw_counts.txt' data obtained from the previous script
       - tcga/maf_files_retriever.sh, retrieves the MAF files for the THCA true primary tumors
       - tcga/thca_clinical_samples_annotation_checks.Rmd, consistency checks
   
Contact details
   mjorda@igtp.cat, mpeinado@igtp.cat (PIs); rbuj@igtp.cat, imallona@igtp.cat (developers)
